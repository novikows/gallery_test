export const BASE_URL = 'http://dev-test.depicture.io/api/'

export const androidCameraPermission = {
  title: 'Permission to use camera',
  message: 'We need your permission to use your camera',
  buttonPositive: 'OK',
  buttonNegative: 'Cancel',
}

export const androidRecordAudioPermission = {
  title: 'Permission to use audio recording',
  message: 'We need your permission to use your audio',
  buttonPositive: 'OK',
  buttonNegative: 'Cancel',
}
