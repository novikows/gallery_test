import React, { PureComponent } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { RNCamera } from 'react-native-camera'
import {
  androidRecordAudioPermission,
  androidCameraPermission,
} from '../constants'
import { uploadPicture } from '../modules/duck'

const Container = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #f5fcff;
`

const CameraPreview = styled(RNCamera)`
  flex: 1;
  width: 100%;
  height: 100%;
  justify-content: flex-end;
  align-items: center;
`

const FooterContainer = styled.View`
  width: 100%;
  height: 96px;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  background-color: #fff;
`

const Button = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
})`
  width: 48px;
  height: 48px;
  border-radius: 18px;
  align-items: center;
  justify-content: space-around;
  background-color: ${({ color }) => (color ? color : '#00c9db88')};
`

class Camera extends PureComponent {
  static navigationOptions = { headerShown: false }

  state = { camera: null, picture: null, isPaused: false }

  takePicture = () => {
    const options = { quality: 0.5, pauseAfterCapture: true }
    this.state.camera
      .takePictureAsync(options)
      .then(this.setPicture)
      .catch(this.resumePreview)

    this.setState({ isPaused: true })
  }

  savePicture = () => {
    const { navigation, uploadPicture } = this.props
    const { picture } = this.state
    if (!!picture) {
      uploadPicture(picture.uri)
      navigation.goBack()
    }
  }

  resumePreview = () => {
    const { picture, camera } = this.state
    if (!!picture) {
      camera.resumePreview()
      this.setState({ picture: null, isPaused: false })
    }
  }

  setCamera = camera => this.setState({ camera })
  setPicture = picture => this.setState({ picture })

  render() {
    return (
      <Container>
        <CameraPreview
          ref={this.setCamera}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.auto}
          androidCameraPermissionOptions={androidCameraPermission}
          androidRecordAudioPermissionOptions={androidRecordAudioPermission}
        />

        <FooterContainer>
          {this.state.isPaused ? (
            <>
              <Button color={'#db3543'} onPress={this.resumePreview} />
              <Button color={'#61ba4a'} onPress={this.savePicture} />
            </>
          ) : (
            <Button onPress={this.takePicture} />
          )}
        </FooterContainer>
      </Container>
    )
  }
}

export default connect(undefined, { uploadPicture })(Camera)
