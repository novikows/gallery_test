import * as R from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  fetchPictures,
  getPicturesList,
  getIsLoadingList,
} from '../modules/duck'
import styled from 'styled-components'
import moment from 'moment'

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #f5fcff;
`

const PicturesList = styled.FlatList`
  width: 100%;
  height: 100%;
  padding-bottom: 56px;
`

const Button = styled.TouchableOpacity`
  position: absolute;
  width: 140px;
  height: 40px;
  bottom: 24px;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
  background-color: #00c9db88;
`

const ButtonText = styled.Text`
  color: #fff;
  font-size: 24px;
  font-weight: 600;
`

const PictureContiner = styled.View`
  width: 100%;
  height: 400px;
  margin: 8px 0;
`

const Photo = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: 100%;
  height: 360px;
`

const PhotoInfo = styled.Text`
  color: #333;
  font-size: 14px;
  font-weight: 600;
  margin: 4px;
`

const FooterContainer = styled.View`
  width: 100%;
  justify-content: flex-start;
  align-items: center;
  height: 84px;
  padding-top: 8px;
`
const Loader = styled.ActivityIndicator``

const Picture = ({ path, name }) => (
  <PictureContiner>
    <Photo source={{ uri: path }} />
    <PhotoInfo>{name.split('_')[2]}</PhotoInfo>
    <PhotoInfo>
      {moment(parseInt(name.split('_')[1])).format('ll HH:mm')}
    </PhotoInfo>
  </PictureContiner>
)

const Footer = ({ isLoading }) => (
  <FooterContainer>{isLoading && <Loader />}</FooterContainer>
)

class Home extends Component {
  componentDidMount() {
    this.props.fetchPictures()
  }

  render() {
    const { navigation, isLoading, picturesList } = this.props
    return (
      <Container>
        <PicturesList
          data={picturesList}
          keyExtractor={item => item.name}
          renderItem={({ item }) => <Picture {...item} />}
          ListFooterComponent={<Footer isLoading={isLoading} />}
        />

        <Button onPress={() => navigation.navigate('Camera')}>
          <ButtonText>CAMERA</ButtonText>
        </Button>
      </Container>
    )
  }
}

export default connect(
  R.applySpec({ picturesList: getPicturesList, isLoading: getIsLoadingList }),
  {
    fetchPictures,
  },
)(Home)
