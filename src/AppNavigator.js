import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { HomeScreen, CameraScreen } from './screens'

const AppNavigator = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Camera: { screen: CameraScreen },
  },
  { initialRouteName: 'Home' },
)

export default createAppContainer(AppNavigator)
