import * as R from 'ramda'
import { combineReducers } from 'redux'
import { createAction, handleAction, handleActions } from 'redux-actions'

export const fetchPictures = createAction('FETCH_PICTURES')
export const setPictures = createAction('SET_PICTURES')
export const fetchPicturesFailure = createAction('FETCH_PICTURES_FAILURE')
export const uploadPicture = createAction('UPLOAD_PICTURE')

const pictures = handleAction(setPictures, (_, { payload }) => payload, [])

const isLoadingList = handleActions(
  {
    [fetchPictures]: R.T,
    [setPictures]: R.F,
    [uploadPicture]: R.T,
    [fetchPicturesFailure]: R.F,
  },
  false,
)

export const getPicturesList = R.prop('pictures')

export const getIsLoadingList = R.prop('isLoadingList')

const reducer = combineReducers({ pictures, isLoadingList })

export default reducer
