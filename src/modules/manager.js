import * as R from 'ramda'
import moment from 'moment'
import { BASE_URL } from '../constants'

const handleStatuses = R.cond([
  [R.propEq('status', 404), () => Promise.reject(new Error('Ошибка данных'))],
  [R.propEq('status', 500), () => Promise.reject(new Error('Ошибка сервера'))],
  [R.T, R.identity],
])

const createFormData = uri => {
  const formData = new FormData()
  formData.append('file', {
    uri,
    type: 'image/jpg',
    name: `${moment().unix()}.jpg`,
  })
  return formData
}

export const getPicturesList = R.pipeP(
  () => fetch(`${BASE_URL}files`),
  handleStatuses,
  res => res.json(),
)

export const uploadPicture = R.pipeP(
  uri =>
    fetch(`${BASE_URL}upload`, {
      method: 'POST',
      headers: { 'Content-Type': 'multipart/form-data' },
      body: createFormData(uri),
    }),
  handleStatuses,
  res => res.status === 200,
)
