import { createStore, applyMiddleware } from 'redux'
import rootReducer from './duck'
import sagaMiddlewareFactory from 'redux-saga'
import rootSaga from './saga'

const sagaMiddleware = sagaMiddlewareFactory()

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(rootSaga)

export default store
