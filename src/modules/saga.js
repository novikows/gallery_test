import { takeLatest, put, call } from 'redux-saga/effects'
import { fetchPictures, setPictures, uploadPicture } from './duck'
import { Alert } from 'react-native'
import * as Manager from './manager'

const fetchPicturesSaga = function*() {
  try {
    const picturesList = yield call(Manager.getPicturesList)
    yield put(setPictures(picturesList))
  } catch (e) {
    Alert.alert('Error', e.message)
    yield put(fetchPicturesFailure())
  }
}

const uploadPictureSaga = function*({ payload: picture }) {
  try {
    const response = yield call(Manager.uploadPicture, picture)
    yield response && call(fetchPicturesSaga)
  } catch (e) {
    Alert.alert('Error', e.message)
    yield put(fetchPicturesFailure())
  }
}

const pictureSaga = function*() {
  yield takeLatest(fetchPictures, fetchPicturesSaga)
  yield takeLatest(uploadPicture, uploadPictureSaga)
}

const rootSaga = function*() {
  yield call(pictureSaga)
}

export default rootSaga
